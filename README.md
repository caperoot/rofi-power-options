## About
 This is to have power options in rofi. There are two files

1. rofi-power
2. power.sh
 
---
 
## rofi-power
 With this file we can have a rofi mode. 
 
### Usage
 If PATH has been set to ~/.local/bin we can copy the file rofi-power to ~/.local/bin and then we can use it as

```
 rofi -show power -modi power:rofi-power
```

 If PATH has not been set then the script can be used as,

```
 rofi -show power -modi power:./path/to/script
```

### Dependencies

 The script works with `systemd`, in addition we need 

1. rofi
2. [betterlockscreen](https://github.com/pavanjadhaw/betterlockscreen)

### Inspiration

 This script is inspired from [rofi-power by Jaakko Luttinen](https://github.com/jluttine/rofi-power-menu)

### Known Issues

 The `Lock Screen` option does not work because lock program fails to grab keyboard & pointer.

---

## power.sh
 This is a standalone script that produces a power option in rofi.

### Usage
 Copy the script to any location then run it.

```
 ./path/to/script/power.sh
```

### Dependencies
 This script works with `systemd`, in addition we need

1. rofi
2. dmenu
3. [betterlockscreen](https://github.com/pavanjadhaw/betterlockscreen)

 
