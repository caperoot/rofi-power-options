#!/usr/bin/bash

MENU="$(rofi -sep "|" -dmenu -p 'System' -width 12 -lines 6 -font "Source Code Pro 13" <<< " Lock| Logout| Suspend| Hibernate| Reboot| Shutdown")"
            case "$MENU" in
                *Lock) betterlockscreen -l blur;;
                *Logout) i3-msg exit;;
		*Suspend) systemctl suspend;;
		*Hibernate) systemctl hibernate;;
                *Reboot) systemctl reboot;;
                *Shutdown) systemctl poweroff
            esac
